package cm.Project;

import java.io.IOException;

import cm.Project.Controller.firstMainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainClass extends Application {
	
	private Stage stage;
	private BorderPane conteneurPrincipal;

	@Override
	public void start(Stage primaryStage) {
		stage = primaryStage;
		stage.setTitle("Bernon Storage");
		
		initialisationconteneurPrincipal();
		initialisationContenu();
		
	}

	private void initialisationconteneurPrincipal(){
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainClass.class.getResource("View/conteneurPrincipal.fxml"));
		try{
			conteneurPrincipal = (BorderPane) loader.load();
			Scene scene = new Scene(conteneurPrincipal);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	

	private void initialisationContenu(){
		FXMLLoader loade = new FXMLLoader();
		loade.setLocation(MainClass.class.getResource("View/FirstMain.fxml"));
		try{
			AnchorPane conteneurHome = (AnchorPane) loade.load();
			conteneurPrincipal.setCenter(conteneurHome);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
