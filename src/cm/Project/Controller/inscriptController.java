package cm.Project.Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

import cm.Project.Database.FirstConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class inscriptController implements Initializable {

    @FXML
    private Pane photo;

    @FXML
    private TextField insNom;

    @FXML
    private TextField insP�re;

    @FXML
    private TextField insPren;

    @FXML
    private TextField insM�re;

    @FXML
    private TextField insLieu;

    @FXML
    private ComboBox<String> insSexe;

    @FXML
    private ComboBox<String> insClasse;

    @FXML
    private ComboBox<String> insS�rie;

    @FXML
    private ComboBox<String> insFrais;

    @FXML
    private TextField insMont;

    @FXML
    private DatePicker insDate;
    
    @FXML
    private TextField insMatri;

    @FXML
    private Button insValid;

    @FXML
    private Button insRes;
    
    String sql = "INSERT INTO ELEVE VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

    @FXML
    void insReset(ActionEvent event) {
    	insMatri.clear();
    	insNom.clear();
    	insPren.clear();
    	insLieu.clear();
    	insP�re.clear();
    	insM�re.clear();
    	
    }

    @FXML
    void insValidation(ActionEvent event) throws ClassNotFoundException, SQLException {
    	Connection con = FirstConnection.Connect();
		PreparedStatement pstmt = con.prepareStatement(sql);
		
		pstmt.setString(1, insMatri.getText());
        pstmt.setString(2, insNom.getText());
        pstmt.setString(3, insPren.getText());
        pstmt.setString(4, insSexe.getValue().toString());
        pstmt.setString(5, insDate.getValue().toString());
        pstmt.setString(6, insLieu.getText());
        pstmt.setString(7, insP�re.getText());
        pstmt.setString(8, insM�re.getText());
        pstmt.setString(9, insClasse.getValue().toString());
        pstmt.setString(10, insS�rie.getValue().toString());
        pstmt.setString(11, insFrais.getValue().toString());
        pstmt.setString(12, insMont.getText());
        pstmt.executeUpdate();
    	
    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		insSexe.getItems().add("Masculin");
		insSexe.getItems().add("Feminin");
		
		insClasse.getItems().add("6eme");
		insClasse.getItems().add("5eme");
		insClasse.getItems().add("4eme");
		insClasse.getItems().add("3eme");
		insClasse.getItems().add("2nde");
		insClasse.getItems().add("Pre");
		insClasse.getItems().add("Tle");
		
		insS�rie.getItems().add("Allemand");
		insS�rie.getItems().add("Espagnol");
		insS�rie.getItems().add("Chinois");
		insS�rie.getItems().add("C");
		insS�rie.getItems().add("D");
		
		insFrais.getItems().add("Inscriptions");
		insFrais.getItems().add("1ere Tranche");
		insFrais.getItems().add("2eme Tranche");
		insFrais.getItems().add("Examen");
	}

}
