package cm.Project.Controller;

import java.io.FileOutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import cm.Project.Classe.Note;
import cm.Project.Database.FirstConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
public class notes1Controller implements Initializable{

    @FXML
    private ComboBox<String> classChoice;

    @FXML
    private ComboBox<String> matiere;
    
    @FXML
    private TableView<Note> tableView;

    @FXML
    private TableColumn<Note, String> notMat;

    @FXML
    private TableColumn<Note, String> notNom;

    @FXML
    private TableColumn<Note, String> notNote;

    @FXML
    private TableColumn<Note, String> noteCoeff;

    @FXML
    private TableColumn<Note, String> noteTotal;

    @FXML
    private TableColumn<Note, String> Apprec;

    @FXML
    private TextField notText;

    @FXML
    private TextField coefText;

    @FXML
    private TextField totText;
    
    @FXML
    private TextField matri;

    @FXML
    private TextField noms;
    
    @FXML
    private Button testButt;
    
    @FXML
    private Button newNote;
    
    @FXML
    private ComboBox<String> apprecCombo;
    
    @FXML
    private Button print;

    private Note nt;
    
    private static final String dir = "Docs/pdf/hello.pdf";
    public Font font  =  FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12);
    public Font font1  =  FontFactory.getFont(FontFactory.HELVETICA, 10);
    public Font font2  =  FontFactory.getFont(FontFactory.COURIER, 10);
    public Font font3  =  FontFactory.getFont(FontFactory.COURIER_BOLD, 10);

    
    ObservableList<Note> stud = FXCollections.observableArrayList();
    
    String query = "select matricule, nom, prenom,note,coef, total, appreciation from eleve inner join notes where notes.noteMatri = eleve.matricule";
    
    @FXML
    void onItemSelected(MouseEvent event) {
    	nt = tableView.getSelectionModel().getSelectedItem();
    	if(nt != null){
    		notText.setText(nt.getNotes());
    		coefText.setText(nt.getCoefs());
    		totText.setText(nt.getTotal());
    		matri.setText(nt.getMatricules());
    		noms.setText(nt.getNoms());
    	}
    }

    
    @FXML
    private String UpdateNotes(TableColumn.CellEditEvent<Note, String> event) {
        Note mc = tableView.getSelectionModel().getSelectedItem();
        mc.setNotes(event.getNewValue());
        System.out.println("new value of notes "+event.getNewValue());
        return event.getNewValue();
    }
    
    @FXML
    private String UpdateCoef(TableColumn.CellEditEvent<Note, String> event) {
        Note mc = tableView.getSelectionModel().getSelectedItem();
        mc.setCoefs(event.getNewValue());
        System.out.println("new value of coefs "+event.getNewValue());
        return event.getNewValue();
    }

    @FXML
    private String UpdateTotal(TableColumn.CellEditEvent<Note, String> event) {
        Note mc = tableView.getSelectionModel().getSelectedItem();
        mc.setTotal(event.getNewValue());
        System.out.println("new value of total "+event.getNewValue());
        return event.getNewValue();
    }

    @FXML
    private String UpdateApprec(TableColumn.CellEditEvent<Note, String> event) {
        Note mc = tableView.getSelectionModel().getSelectedItem();
        mc.setAppreciation(event.getNewValue());
        System.out.println("new value of apprec "+event.getNewValue());
        return event.getNewValue();
    }
    
  
    
    @FXML
    void printNotes(ActionEvent event) throws Exception {
    	
    	//String url = "Docs/pdf/xple.pdf";
    	
    		
    	String field = "Matricule : "+matri.getText()+"        Nom et pr�noms : "+noms.getText()+" \n"
    			+ "Ann�e scolaire : 2019/2020        Classe :"+classChoice.getValue()+"\n"
    			+ "Date de naissance : 07/02/2001    Lieu de naissance : Yaound� \n "
    			+ "Redoublant :            Professeur Principal :     ";
    	
    	Paragraph para = new Paragraph(field,font1);
    	
    	
    	
    	//Ajout d'un paargraphe au document

    	/*Rectangle rect = new Rectangle(210,160);
    	FileOutputStream fos = new FileOutputStream(new File(dir));
    	 
        PdfReader pdfReader = new PdfReader(url);
        PdfStamper sta = new PdfStamper(pdfReader, fos);
        for(int i=1; i<= pdfReader.getNumberOfPages(); i++)
		{
			PdfContentByte content = sta.getUnderContent(i);
			
			
		}

		sta.close();*/
    	Document doc = new Document();
    		PdfWriter.getInstance(doc, new FileOutputStream(dir));
    		doc.open();
    		
    		doc.add(para);
    		
    		doc.add(premierTableau());
    		doc.add(secondTableau());
    		doc.add(troisiemeTableau());
    		doc.add(quartTableau());
    		doc.add(cinqTableau());
    		doc.close();
    	
    }

    
    public  PdfPTable premierTableau()
    {
    	PdfPTable table = new PdfPTable(5);
    	try{
			String sq = "SELECT matiere, note, coef, total, appreciation FROM NOTES WHERE noteMatri = '"+matri.getText()+"' AND type = 'Litt�raire'";
			String sq1 = "SELECT matiere, note, coef, total, appreciation FROM NOTES WHERE noteMatri = '"+matri.getText()+"' AND type = 'Scientifique'";
			String sq2 = "SELECT matiere, note, coef, total, appreciation FROM NOTES WHERE noteMatri = '"+matri.getText()+"' AND type = 'Autre'";
			String sq3 = "SELECT SUM(coef) as l from notes where noteMatri = '"+matri.getText()+"' AND type = 'Litt�raire'";
		   	String sq4 = "SELECT SUM(coef) as s from notes where noteMatri = '"+matri.getText()+"' AND type = 'Scientifique'";
		   	String sq5 = "SELECT SUM(coef) as a from notes where noteMatri = '"+matri.getText()+"' AND type = 'Autre'";
		   	String sql = "SELECT SUM(total) as tol from notes where noteMatri = '"+matri.getText()+"' AND type = 'Litt�raire'";
		   	String sql1 = "SELECT SUM(total) as tos from notes where noteMatri = '"+matri.getText()+"' AND type = 'Scientifique'";
		   	String sql2 = "SELECT SUM(total) as toa from notes where noteMatri = '"+matri.getText()+"' AND type = 'Autre'";
		    String t = "SELECT ROUND(SUM(total)/SUM(coef),2) AS a FROM notes WHERE noteMatri = '"+matri.getText()+"' AND type = 'litt�raire'";
		    String u = "SELECT ROUND(SUM(total)/SUM(coef),2) AS a FROM notes WHERE noteMatri = '"+matri.getText()+"' AND type = 'scientifique'";
		    String v = "SELECT ROUND(SUM(total)/SUM(coef),2) AS a FROM notes WHERE noteMatri = '"+matri.getText()+"' AND type = 'autre'";
		    
		    
	    	Connection cn = FirstConnection.Connect();
	        //On cr�er un objet table dans lequel on intialise �a taille.
	        PreparedStatement pt = null, pt1 = null, pt2 = null;
	        ResultSet st = null, st1 = null, st2 = null;
	        
	        pt = cn.prepareStatement(sq);
	        st = pt.executeQuery();
	        pt1 = cn.prepareStatement(sq1);
	        st1 = pt1.executeQuery();
	        pt2 = cn.prepareStatement(sq2);
	        st2 = pt2.executeQuery();
	   
	        
	       
	        PdfPCell cl = new PdfPCell(new Phrase("Mati�res Litt�raires",font));
	        PdfPCell cl1 = new PdfPCell(new Phrase("Mati�res Scientifiques",font));
	        PdfPCell cl2 = new PdfPCell(new Phrase("Mati�res Autres",font));
	        PdfPCell cell = new PdfPCell(new Phrase("Mati�res",font));
	        PdfPCell cell1 = new PdfPCell(new Phrase("Notes",font));
	        PdfPCell cell2 = new PdfPCell(new Phrase("Coefficient",font));
	        PdfPCell cell3 = new PdfPCell(new Phrase("Total",font));
	        PdfPCell cell4 = new PdfPCell(new Phrase("Appreciation",font));
	        

	        
	        cl.setColspan(5);
	        cl1.setColspan(5);
	        cl2.setColspan(5);
	        table.addCell(cell);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        table.addCell(cell4);
	        table.addCell(cl);
	        
	        
	        while(st.next()){
	        	
		        table.addCell(st.getString("matiere"));
		        table.addCell(st.getString("note"));
		        table.addCell(st.getString("coef"));
		        table.addCell(st.getString("total"));
		        table.addCell(st.getString("appreciation"));
	        }
	    
	        PreparedStatement psmt1, pst, pst1,  psm1, ps, ps1, p, p1, p2;
			ResultSet res2,res3,res, es2,es3,es, s, s2, s3;
			psmt1 = cn.prepareStatement(sq3);			
			res2 = psmt1.executeQuery();
			pst = cn.prepareStatement(sql);			
			res3 = pst.executeQuery();
			pst1 = cn.prepareStatement(t);			
			res = pst1.executeQuery();
			psm1 = cn.prepareStatement(sq4);			
			es2 = psm1.executeQuery();
			ps = cn.prepareStatement(sql1);			
			es3 = ps.executeQuery();
			ps1 = cn.prepareStatement(u);			
			es = ps1.executeQuery();
			p = cn.prepareStatement(sq5);			
			s2 = p.executeQuery();
			p1 = cn.prepareStatement(sql2);			
			s3 = p1.executeQuery();
			p2 = cn.prepareStatement(v);			
			s = p2.executeQuery();
			
			
			while(res2.next()){
				String sum = res2.getString("l");
				PdfPCell b = new PdfPCell(new Phrase("Totaux litt�raires",font));
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				b.setColspan(2);
				table.addCell(b);
				table.addCell(c);
			}
			
			while(res3.next()){
				String sum = res3.getString("tol");
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				table.addCell(c);
			}
			
			while(res.next()){
				String si = res.getString("a");
				PdfPCell c = new PdfPCell(new Phrase(si,font));
				table.addCell(c);
			}
			
			res2.close();
			res3.close();
			res.close();
	        
	       table.addCell(cl1);
	       while(st1.next()){
	        	
		        table.addCell(st1.getString("matiere"));
		        table.addCell(st1.getString("note"));
		        table.addCell(st1.getString("coef"));
		        table.addCell(st1.getString("total"));
		        table.addCell(st1.getString("appreciation"));
	        }
			
			while(es2.next()){
				String sum = es2.getString("s");
				PdfPCell b = new PdfPCell(new Phrase("Totaux Scientifiques",font));
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				b.setColspan(2);
				table.addCell(b);
				table.addCell(c);
			}
			
			while(es3.next()){
				String sum = es3.getString("tos");
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				table.addCell(c);
			}
			
			while(es.next()){
				String so = es.getString("a");
				PdfPCell c = new PdfPCell(new Phrase(so,font));
				table.addCell(c);
			}
			
			es2.close();
			es3.close();
			es.close();
	       
	       table.addCell(cl2);
	       while(st2.next()){
	        	
		        table.addCell(st2.getString("matiere"));
		        table.addCell(st2.getString("note"));
		        table.addCell(st2.getString("coef"));
		        table.addCell(st2.getString("total"));
		        table.addCell(st2.getString("appreciation"));
	        }
	       while(s2.next()){
				String sum = s2.getString("a");
				PdfPCell b = new PdfPCell(new Phrase("Totaux Autres",font));
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				b.setColspan(2);
				table.addCell(b);
				table.addCell(c);
			}
			
			while(s3.next()){
				String sum = s3.getString("toa");
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				table.addCell(c);
			}
			
			while(s.next()){
				String huf = s.getString("a");
				PdfPCell c = new PdfPCell(new Phrase(huf,font));
				table.addCell(c);
			}
			
			es2.close();
			es3.close();
			es.close();
	        } catch(Exception e){
	        	e.printStackTrace();
	        }      
       
    	 return table; 
    }
    
private PdfPTable secondTableau(){
    	
    	PdfPTable scd = new PdfPTable(6);
    	PdfPCell cl = new PdfPCell(new Phrase("Travail",font3));
        PdfPCell cl1 = new PdfPCell(new Phrase("Total",font3));
        PdfPCell cl2 = new PdfPCell(new Phrase("Moyenne",font3));
        PdfPCell cell = new PdfPCell(new Phrase("Rang",font3));
        PdfPCell cell1 = new PdfPCell(new Phrase("Moyenne g�n�rale",font3));
        PdfPCell cell2 = new PdfPCell(new Phrase("Appr�ciation",font3));
        
        scd.addCell(cl);
        scd.addCell(cl1);
        scd.addCell(cl2);
        scd.addCell(cell);
        scd.addCell(cell1);
        scd.addCell(cell2);
        
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cl.setHorizontalAlignment(Element.ALIGN_CENTER);
        cl1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cl2.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        cl.setBorderWidth(2);
        cl1.setBorderWidth(2);
        cl2.setBorderWidth(2);
        cell.setBorderWidth(2);
        cell1.setBorderWidth(2);
        cell2.setBorderWidth(2);
        

    	return scd;
    }

    public PdfPTable troisiemeTableau(){
    	String sql3 = "SELECT round(SUM(total),2) as tot from notes WHERE noteMatri = '"+matri.getText()+"'";
    	String w = "SELECT ROUND(SUM(total)/SUM(coef),2) AS a FROM notes WHERE noteMatri = '"+matri.getText()+"'";
    	String moy = "update eleve set moyenne = (select(sum(total)/sum(coef)) from notes where notematri = '"+matri.getText()+"') where matricule = '"+matri.getText()+"'";
    	String rg = "select round(avg(moyenne),2) as cel from eleve where classe = '"+classChoice.getValue().toString()+"'";
    	
    	PdfPTable scd = new PdfPTable(6);
    	try {
    		Connection conect = FirstConnection.Connect();
    		PreparedStatement sot = null, sot1 = null, nw = null,gof = null;
    		ResultSet ros = null, ros1 = null, fog = null;
    		
    		sot = conect.prepareStatement(sql3);
    		ros = sot.executeQuery();
    		sot1 = conect.prepareStatement(w);
    		ros1 = sot1.executeQuery();
    		nw = conect.prepareStatement(moy);
    		gof = conect.prepareStatement(rg);
    		fog = gof.executeQuery();
    		int j = nw.executeUpdate(moy);
    		
    		if (j == 1){
    			System.out.println("Valeur ajout�e avec succ�s");
    		}
    		
    		PdfPCell l = new PdfPCell(new Phrase(""));
    		PdfPCell cl = new PdfPCell(new Phrase("3e",font3));
    		PdfPCell app = new PdfPCell(new Phrase("Assez-Bien",font3));
    		
	        l.setBorder(Rectangle.NO_BORDER);
	        cl.setBorderWidth(1);
	        scd.addCell(l);
	        
	        while(ros.next()){
				String sum = ros.getString("tot");
				PdfPCell c = new PdfPCell(new Phrase(sum,font3));
				c.setBorderWidth(1);
			    c.setHorizontalAlignment(Element.ALIGN_CENTER);
				scd.addCell(c);
			}
			
			while(ros1.next()){
				String sum = ros1.getString("a");
				PdfPCell c = new PdfPCell(new Phrase(sum,font3));
				c.setBorderWidth(1);
			    c.setHorizontalAlignment(Element.ALIGN_CENTER);
				scd.addCell(c);
			}
			
			scd.addCell(cl);
	        cl.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			while (fog.next()){
				String sum = fog.getString("cel");
		        PdfPCell cl1 = new PdfPCell(new Phrase(sum,font3));
		        cl1.setBorderWidth(1);
		        cl1.setHorizontalAlignment(Element.ALIGN_CENTER);
		        scd.addCell(cl1);

			 }
			app.setBorderWidth(1);
			scd.addCell(app);
				/*String sum = ros1.getString("a");
				String h  = null;
			    if(Integer.parseInt(sum) >= 0 || Integer.parseInt(sum) <= 1.99){
					h = "Nulle";
				} else if(Integer.parseInt(sum) >= 2 || Integer.parseInt(sum) <= 3.99){
					h = "Tr�s-Faible";
				} else if(Integer.parseInt(sum) >= 4 || Integer.parseInt(sum) <= 5.99){
					h = "Faible";
				} else if(Integer.parseInt(sum) >= 6 || Integer.parseInt(sum) <= 7.99){
					h = "Insuffisant";
				} else if(Integer.parseInt(sum) >= 8 || Integer.parseInt(sum) <= 9.99){
					h = "Mediocre";
				}  else if(Integer.parseInt(sum) >= 10 || Integer.parseInt(sum) <= 11.99){
					h = "Passable";
				} else if(Integer.parseInt(sum) >= 12 || Integer.parseInt(sum) <= 13.99){
					h = "Assez-Bien";
				} else if(Integer.parseInt(sum) >= 14 || Integer.parseInt(sum) <= 15.99){
					h = "Bien";
				} else if(Integer.parseInt(sum) >= 16 || Integer.parseInt(sum) <= 17.99){
					h = "Tr�s-Bien";
				} else if(Integer.parseInt(sum) >= 18 || Integer.parseInt(sum) <= 19.99){
					h = "Excellent";
				} else if(Integer.parseInt(sum) == 20){
					h = "Parfait";
				} 
			    PdfPCell c = new PdfPCell(new Phrase(sum,font3));
				c.setBorderWidth(1);
			    c.setHorizontalAlignment(Element.ALIGN_CENTER);
				scd.addCell(c);*/
			
			
		
			ros.close();
			ros1.close();
			fog.close();
			l.setHorizontalAlignment(Element.ALIGN_CENTER);
    		
    	} catch(Exception e){
    		e.printStackTrace();
    	}
    	return scd;
    }
    
    private PdfPTable quartTableau(){
    	
    	PdfPTable scd = new PdfPTable(6);
    	PdfPCell cl = new PdfPCell(new Phrase("Discipline",font3));
        PdfPCell cl1 = new PdfPCell(new Phrase("Avertissement",font3));
        PdfPCell cl2 = new PdfPCell(new Phrase("Bl�me",font3));
        PdfPCell cell = new PdfPCell(new Phrase("Exclusions (Jrs)",font3));
        PdfPCell cell1 = new PdfPCell(new Phrase("Absences (Hrs)",font3));
        PdfPCell cell2 = new PdfPCell(new Phrase("Retards",font3));
        
        scd.addCell(cl);
        scd.addCell(cl1);
        scd.addCell(cl2);
        scd.addCell(cell);
        scd.addCell(cell1);
        scd.addCell(cell2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cl.setHorizontalAlignment(Element.ALIGN_CENTER);
        cl1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cl2.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        cl.setBorderWidth(1);
        cl1.setBorderWidth(1);
        cl2.setBorderWidth(1);
        cell.setBorderWidth(1);
        cell1.setBorderWidth(1);
        cell2.setBorderWidth(1);
    	return scd;
    }

    public PdfPTable cinqTableau(){
    	//String sql3 = "SELECT SUM(total) as tot from notes WHERE noteMatri = '"+matri.getText()+"'";
    	//String w = "SELECT ROUND(SUM(total)/SUM(coef),2) AS a FROM notes WHERE noteMatri = '"+matri.getText()+"'";
    	PdfPTable scd = new PdfPTable(6);
    	try {
    		/*Connection conect = FirstConnection.Connect();
    		PreparedStatement sot = null, sot1 = null;
    		ResultSet ros = null, ros1 = null;
    		
    		sot = conect.prepareStatement(sql3);
    		ros = sot.executeQuery();
    		sot1 = conect.prepareStatement(w);
    		ros1 = sot1.executeQuery();*/
    		
    		PdfPCell a = new PdfPCell(new Phrase(""));
    		PdfPCell c = new PdfPCell(new Phrase("1",font3));
    		PdfPCell l = new PdfPCell(new Phrase("0",font3));
    		PdfPCell cl = new PdfPCell(new Phrase("0",font3));
	        PdfPCell cl1 = new PdfPCell(new Phrase("5",font3));
	        PdfPCell cl2 = new PdfPCell(new Phrase("2",font3));
	        
	        a.setBorder(Rectangle.NO_BORDER);
	        c.setBorderWidth(1);
	        l.setBorderWidth(1);
	        cl.setBorderWidth(1);
	        cl1.setBorderWidth(1);
	        cl2.setBorderWidth(1);
	        
	        c.setHorizontalAlignment(Element.ALIGN_CENTER);
	        l.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cl.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cl1.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cl2.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
	        scd.addCell(a);
	        scd.addCell(c);
	        scd.addCell(l);
			scd.addCell(cl);
	        scd.addCell(cl1);
	        scd.addCell(cl2);
	        
	        /*while(ros.next()){
				String sum = ros.getString("tot");
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				c.setBorderWidth(1);
				scd.addCell(c);
			}
			
			while(ros1.next()){
				String sum = ros1.getString("a");
				PdfPCell c = new PdfPCell(new Phrase(sum,font));
				c.setBorderWidth(1);
				scd.addCell(c);
			}
			ros.close();
			ros1.close();*/
    		
    	} catch(Exception e){
    		e.printStackTrace();
    	}
    	return scd;
    }
    
    @FXML
    void PrintData(ActionEvent event) {
    	String a =  getValueAt(tableView,0,0);
		System.out.println(a);
		String sql = "UPDATE notes SET note = ?, coef = ?, total = ?, appreciation = ?  WHERE noteMatri = '"+matri.getText()+"' AND matiere = '"+matiere.getValue().toString()+"' ";
		
		Connection con;
		try {
			con = FirstConnection.Connect();
			PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1, notText.getText());
			pst.setString(2, coefText.getText());
			pst.setString(3, totText.getText());
			pst.setString(4, apprecCombo.getValue().toString());
			
			int i = pst.executeUpdate();
			
			if(i == 1){
				Alert alert = new Alert(AlertType.INFORMATION);
	    		alert.setTitle("Enegistrement d'�l�ments");
	    		alert.setHeaderText("Mise � jour effectu�e");
	    		alert.showAndWait();
			}
			pst.close();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		stud.clear();

		
		
		initialize(null,null);
		
		/*String sql = "UPDATE NOTES set note = '"+UpdateNotes()+"'  where noteMatri = '"+a+"'";
		
		Connection con;
		try {
			con = FirstConnection.Connect();
			PreparedStatement pst = con.prepareStatement(sql);
			int i = pst.executeUpdate();
			
			if(i == 1){
				Alert alert = new Alert(AlertType.INFORMATION);
	    		alert.setTitle("Mise � jour effectu�e");
	    		alert.setHeaderText("Enregistrement d'�l�ments");
	    		alert.showAndWait();
			}
			pst.close();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
    }
    
    @FXML
    void goToNewNote(ActionEvent event) {
    	
    	try{
    		FXMLLoader loade = new FXMLLoader(getClass().getResource("/cm/Project/View/newNote.fxml"));
    		VBox root = (VBox) loade.load();
    		Stage stage = new Stage();
    		stage.setTitle("Bernon Storage");
    		stage.setScene(new Scene(root));
    		stage.show();
    	} catch (Exception e){
    		e.printStackTrace();
    	}

    }
    
    
     public void conboChoice(){
    	String sql1 = "select matricule, nom, prenom,note,coef, total, appreciation from eleve inner join notes where notes.noteMatri = eleve.matricule and eleve.classe = '"+classChoice.getValue()+"' and notes.matiere = '"+matiere.getValue()+"'";
		
		stud.clear();
		try {
			Connection con = FirstConnection.Connect();
			PreparedStatement pst = con.prepareStatement(sql1);
			ResultSet res = pst.executeQuery(sql1);
			while(res.next()){
				stud.add(new Note(res.getString("matricule"),res.getString("nom"),res.getString("note"), res.getString("coef"),res.getString("total"),res.getString("appreciation")));				}
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		apprecCombo.getItems().add("Nulle");
		apprecCombo.getItems().add("M�diocre");
		apprecCombo.getItems().add("Insuffisant");
		apprecCombo.getItems().add("Passable");
		apprecCombo.getItems().add("Assez-Bien");
		apprecCombo.getItems().add("Bien");
		apprecCombo.getItems().add("Tr�s bien");
		apprecCombo.getItems().add("Excellent");
		apprecCombo.getItems().add("Parfait");
		
		classChoice.getItems().add("6eme");
		classChoice.getItems().add("5eme");
		classChoice.getItems().add("4eme");
		classChoice.getItems().add("3eme");
		classChoice.getItems().add("2nde");
		classChoice.getItems().add("Pre");
		classChoice.getItems().add("Tle");
		classChoice.getItems().add("");
		
		matiere.getItems().add("Math�matiques");
		matiere.getItems().add("Physiques");
		matiere.getItems().add("Chimie");
		matiere.getItems().add("S.V.T");
		matiere.getItems().add("Informatique");
		matiere.getItems().add("Fran�ais");
		matiere.getItems().add("Anglais");
		matiere.getItems().add("Histoire");
		matiere.getItems().add("G�ographie");
		matiere.getItems().add("E.C.M");
		matiere.getItems().add("Allemand");
		matiere.getItems().add("Espagnol");
		matiere.getItems().add("Chinois");
		matiere.getItems().add("Philosophie");
		matiere.getItems().add("E.P.S");
		matiere.getItems().add("Agriculture");
		matiere.getItems().add("El�vage");
		matiere.getItems().add("Couture");
		matiere.getItems().add("Artisanat");
		matiere.getItems().add("C.V.E.R");
		matiere.getItems().add("T.M");
		matiere.getItems().add("Gestion");
		
		
		try {
			Connection con = FirstConnection.Connect();
			ResultSet res = con.createStatement().executeQuery(query);
			while(res.next()){
				stud.add(new Note(res.getString("matricule"),res.getString("nom"),res.getString("note"), res.getString("coef"),res.getString("total"),res.getString("appreciation")));
			}
			res.close();
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		notMat.setCellValueFactory(new PropertyValueFactory<Note, String>("matricules"));
		notNom.setCellValueFactory(new PropertyValueFactory<Note, String>("noms"));
		notNote.setCellValueFactory(new PropertyValueFactory<Note, String>("notes"));
		notNote.setCellFactory(TextFieldTableCell.<Note>forTableColumn());
		noteCoeff.setCellValueFactory(new PropertyValueFactory<Note, String>("coefs"));
		noteCoeff.setCellFactory(TextFieldTableCell.<Note>forTableColumn());
		noteTotal.setCellValueFactory(new PropertyValueFactory<Note, String>("total"));
		noteTotal.setCellFactory(TextFieldTableCell.<Note>forTableColumn());
		Apprec.setCellValueFactory(new PropertyValueFactory<Note, String>("appreciation"));
		Apprec.setCellFactory(TextFieldTableCell.<Note>forTableColumn());
    	
    	tableView.setItems(stud);
    	
		
	}
	
	public String getValueAt(TableView<Note> table, int column, int row){
		return table.getColumns().get(column).getCellObservableValue(row).getValue().toString();
	}
	
}
