package cm.Project.Controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;

import cm.Project.Database.FirstConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class firstMainController implements Initializable{

    @FXML
    private TextField userName;

    @FXML
    private PasswordField userPass;

    @FXML
    private TextField name;

    @FXML
    private TextField lastName;

    @FXML
    private TextField birthPlace;

    @FXML
    private ComboBox<String> sexe;

    @FXML
    private ComboBox<String> categ;

    @FXML
    private ComboBox<String> poste;

    @FXML
    private ComboBox<String> departement;

    @FXML
    private DatePicker birthDate;

    @FXML
    private TextField nCNI;

    @FXML
    private ComboBox<String> subject;

    @FXML
    private Button valid;

    @FXML
    private Button annule;
    
    String sql = "INSERT INTO INSCRIPTENSEIGNANT VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

    @FXML
    void Erase(ActionEvent event) {
    	System.exit(0);

    }

    @FXML
    void Validation(ActionEvent event) {
    	
    	/*if(userName.getText().isEmpty()){
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setTitle("Empty field");
    		alert.setHeaderText("Username field is empty");
    		alert.show();
    	} 
    	else if (userPass.getText().isEmpty()){
    		System.out.println("Please fill empty field");
    		if(userPass.getLength() < 8){
    			Alert alert = new Alert(AlertType.ERROR);
        		alert.setTitle("Empty field");
        		alert.setHeaderText("Password lenght as unleast of 8 field and is empty");
        		alert.showAndWait();
    		}
    	}else if(name.getText().isEmpty()){
    		System.out.println("Please fill empty field");
    	}else if(lastName.getText().isEmpty()){
    		System.out.println("Please fill empty field");
    	} else if(birthPlace.getText().isEmpty()){
    		System.out.println("Please fill empty field");
    	} else {*/
    		try {
    			/*
    			Connection con = FirstConnection.Connect();
        		PreparedStatement pstmt = con.prepareStatement(sql);
        		
        		pstmt.setString(1, userName.getText());
                pstmt.setString(2, userPass.getText());
                pstmt.setString(3, name.getText());
                pstmt.setString(4, lastName.getText());
                pstmt.setString(5, birthPlace.getText());
                pstmt.setString(6, nCNI.getText());
                pstmt.setString(7, sexe.getValue().toString());
                pstmt.setString(8, birthDate.getValue().toString());
                pstmt.setString(9, categ.getValue().toString());
                pstmt.setString(10, subject.getValue().toString());
                pstmt.setString(11, poste.getValue().toString());
                pstmt.setString(12, departement.getValue().toString());
                pstmt.executeUpdate();*/
                
        		Parent loade = FXMLLoader.load(getClass().getResource("/cm/Project/View/Home.fxml"));
        		Scene scene2 = new Scene(loade);
    			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    			window.setScene(scene2);
    			window.show();   		
        		
        	} catch (Exception e){
        		e.printStackTrace();
        	}
    	}
    
		
    //}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		sexe.getItems().add("Masculin");
		sexe.getItems().add("Feminin");
		
		categ.getItems().add("Administratif");
		categ.getItems().add("Enseignant");
		categ.getItems().add("Assistant");
		
		subject.getItems().add("Mathématiques");
		subject.getItems().add("Physiques-Chimie");
		subject.getItems().add("S.V.T");
		subject.getItems().add("Informatique");
		subject.getItems().add("Français");
		subject.getItems().add("Anglais");
		subject.getItems().add("Histoire-Géographie-E.C.M");
		subject.getItems().add("Allemand");
		subject.getItems().add("Espagnol");
		subject.getItems().add("Chinois");
		subject.getItems().add("Philosophie");
		subject.getItems().add("E.P.S");
		subject.getItems().add("Autres");
		
		departement.getItems().add("Mathématiques");
		departement.getItems().add("Physiques-Chimie");
		departement.getItems().add("S.V.T");
		departement.getItems().add("Informatique");
		departement.getItems().add("Français");
		departement.getItems().add("Anglais");
		departement.getItems().add("Histoire-Géographie-E.C.M");
		departement.getItems().add("Allemand");
		departement.getItems().add("Espagnol");
		departement.getItems().add("Chinois");
		departement.getItems().add("Philosophie");
		departement.getItems().add("E.P.S");
		departement.getItems().add("Autres");
		
		poste.getItems().add("Enseignant");
		poste.getItems().add("Surveillant");
		poste.getItems().add("Chef de département");
		poste.getItems().add("Conseiller d'orientation");
		poste.getItems().add("Principal");
	}

}

