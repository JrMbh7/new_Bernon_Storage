package cm.Project.Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

import cm.Project.Database.FirstConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class newNoteController implements Initializable{

    @FXML
    private ComboBox<String> classChoice;

    @FXML
    private TextField matri;

    @FXML
    private Button newValid;

    @FXML
    private Button newRes;

    @FXML
    private ComboBox<String> matiere;
    
    @FXML
    private TextField coef;

    @FXML
    private TextField note;

    @FXML
    private ComboBox<String> apprecCombo;

    @FXML
    private TextField total;

    @FXML
    private ComboBox<String> type;


    @FXML
    void insReset(ActionEvent event) {
    	matri.clear();
    	total.clear();
    	coef.clear();
    	apprecCombo.setValue("");
    	classChoice.setValue("");
    	note.clear();

    }

    @FXML
    void insValidation(ActionEvent event) throws SQLException, ClassNotFoundException {
    	String sql = "INSERT INTO NOTES VALUES (?,?,?,?,?,?,?)";
    	Connection con = FirstConnection.Connect();
		PreparedStatement pstmt = con.prepareStatement(sql);
		
		pstmt.setString(1, matiere.getValue().toString());
        pstmt.setString(2, matri.getText());
        pstmt.setString(3, note.getText());
        pstmt.setString(4, coef.getText());
        pstmt.setString(5, total.getText());
        pstmt.setString(6, apprecCombo.getValue().toString());
        pstmt.setString(7, type.getValue().toString());
       int i = pstmt.executeUpdate();
       if(i == 1){
    	   Alert alert = new Alert(AlertType.INFORMATION);
   		alert.setTitle("Enegistrement d'�l�ments");
   		alert.setHeaderText("Nouvelle note enregistr�e");
   		alert.show();
       }
       pstmt.close();

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		matiere.getItems().add("Math�matiques");
		matiere.getItems().add("Physiques");
		matiere.getItems().add("Chimie");
		matiere.getItems().add("S.V.T");
		matiere.getItems().add("Informatique");
		matiere.getItems().add("Fran�ais");
		matiere.getItems().add("Anglais");
		matiere.getItems().add("Histoire");
		matiere.getItems().add("G�ographie");
		matiere.getItems().add("E.C.M");
		matiere.getItems().add("Allemand");
		matiere.getItems().add("Espagnol");
		matiere.getItems().add("Chinois");
		matiere.getItems().add("Philosophie");
		matiere.getItems().add("E.P.S");
		matiere.getItems().add("Agriculture");
		matiere.getItems().add("El�vage");
		matiere.getItems().add("Couture");
		matiere.getItems().add("Artisanat");
		matiere.getItems().add("C.V.E.R");
		matiere.getItems().add("T.M");
		matiere.getItems().add("Gestion");
		
		type.getItems().add("Scientifique");
		type.getItems().add("Litt�raire");
		type.getItems().add("Autre");
		
		classChoice.getItems().add("6eme");
		classChoice.getItems().add("5eme");
		classChoice.getItems().add("4eme");
		classChoice.getItems().add("3eme");
		classChoice.getItems().add("2nde");
		classChoice.getItems().add("Pre");
		classChoice.getItems().add("Tle");
		classChoice.getItems().add("");
		
		apprecCombo.getItems().add("Nulle");
		apprecCombo.getItems().add("M�diocre");
		apprecCombo.getItems().add("Insuffisant");
		apprecCombo.getItems().add("Passable");
		apprecCombo.getItems().add("Assez-Bien");
		apprecCombo.getItems().add("Bien");
		apprecCombo.getItems().add("Tr�s bien");
		apprecCombo.getItems().add("Excellent");
		apprecCombo.getItems().add("Parfait");
		
	}

}
