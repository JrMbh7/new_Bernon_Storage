package cm.Project.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class homeController implements Initializable{

    @FXML
    private Button inscript;

    @FXML
    private Button noteManag;

    @FXML
    private Button studManag;

    @FXML
    private Button eventManag;

    @FXML
    private Button staffManag;

    @FXML
    private Button Inform;
    
    @FXML
    private void handleButtonAction(javafx.event.ActionEvent mouseEvent) {
    	if(mouseEvent.getSource() == Inform){
    		loadStage("/cm/Project/View/Informations.fxml",mouseEvent );
    	} else if(mouseEvent.getSource() == inscript){
    		loadStage("/cm/Project/View/Inscription.fxml",mouseEvent);
    	} else if(mouseEvent.getSource() == noteManag){
    		loadStage("/cm/Project/View/Notes1.fxml",mouseEvent);
    	} else if(mouseEvent.getSource() == studManag){
    		loadStage("/cm/Project/View/Student.fxml",mouseEvent);
    	} else if(mouseEvent.getSource() == eventManag){
    		loadStage("",mouseEvent);
    	} else if(mouseEvent.getSource() == staffManag){
    		loadStage("/cm/Project/View/Staff.fxml",mouseEvent);
    	} 

    }

    private void loadStage(String fxml,javafx.event.ActionEvent event){
		try {
			Parent loade = FXMLLoader.load(getClass().getResource(fxml));
    		Scene scene2 = new Scene(loade);
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			window.setScene(scene2);
			window.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    @FXML
    private void uit(ActionEvent event){
    	
    }
    @FXML
    private void classTotal(ActionEvent event){
    	
    }
    @FXML
    private void staffTotal(ActionEvent event){
    	
    }
    @FXML
    private void totTotal(ActionEvent event){
    	
    }
    @FXML
    private void classTaux(ActionEvent event){
    	
    }
    @FXML
    private void totTaux(ActionEvent event){
    	
    }
    @FXML
    private void studSearch(ActionEvent event){
    	
    }
    @FXML
    private void staffSearch(ActionEvent event){
    	
    }
    @FXML
    private void noteSearch(ActionEvent event){
    	
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
