package cm.Project.Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;   
import cm.Project.Classe.Registration;
import cm.Project.Database.FirstConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

public class staffController implements Initializable{

    @FXML
    private Pane teachStaff;

    @FXML
    private Label fiilTeach;

    @FXML
    private Pane adminStaff;

    @FXML
    private Label fillAdmin;

    @FXML
    private Pane assistStaff;

    @FXML
    private Label fillAssist;

    @FXML
    private Pane Departments;

    @FXML
    private Label fillDepart;
    
    @FXML
    private TableView<Registration> tableView;

    @FXML
    private TableColumn<Registration, String> mat;

    @FXML
    private TableColumn<Registration, String> nom;

    @FXML
    private TableColumn<Registration, String> prenom;

    @FXML
    private TableColumn<Registration, String> sex;

    @FXML
    private TableColumn<Registration, String> lieu;

    @FXML
    private TableColumn<Registration, String> poste;

    @FXML
    private TableColumn<Registration, LocalDate> date;

    @FXML
    private ComboBox<String> classStaff;

    @FXML
    private Button details;

    ObservableList<Registration> stud = FXCollections.observableArrayList();
    
    
    String count = "SELECT COUNT(*) FROM INSCRIPTENSEIGNANT where categorie = 'Enseignant'";
    String count1 = "SELECT COUNT(*) FROM INSCRIPTENSEIGNANT WHERE  categorie = 'Administratif'";
    String count2 = "SELECT COUNT(*) FROM INSCRIPTENSEIGNANT WHERE categorie = 'Assistant'";

	public int numFill(){
		int number = 0;
		try {
			
			Connection con = FirstConnection.Connect();
			PreparedStatement psmt;
			ResultSet res1;
			psmt = con.prepareStatement(count);
			res1 = psmt.executeQuery(count);
			
			while(res1.next()){
				number = res1.getInt("count(*)");
			}

			res1.close();	
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		fiilTeach.setText(Integer.toString(number));
		return number;
	}
	
	
	public int numFill1(){
		int number = 0;
		try {
			
			Connection con = FirstConnection.Connect();
			PreparedStatement psmt1;
			ResultSet res2;
			psmt1 = con.prepareStatement(count1);			
			res2 = psmt1.executeQuery(count1);
			
			while(res2.next()){
				number = res2.getInt("count(*)");
			}
			
			res2.close();
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		fillAdmin.setText(Integer.toString(number));
		return number;
	}
	
	public int numFill2(){
		int number = 0;
		try {
			
			Connection con = FirstConnection.Connect();
			PreparedStatement psmt;
			ResultSet res1;
			psmt = con.prepareStatement(count2);
			
			res1 = psmt.executeQuery(count2);
			
			while(res1.next()){
				number = res1.getInt("count(*)");
			}

			res1.close();	
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		fillAssist.setText(Integer.toString(number));
		return number;
	}
	
	/*public void comboChoice(){

		String sql1 = "SELECT * FROM INSCRIPTENSEIGNANT WHERE Classe = '"+classStaff.getValue()+"';";
		
		if(studClass.getValue().toString().equals("")){
			initialize(null, null);
		} else {
			stud.clear();
			try {
				Connection con = FirstConnection.Connect();
				PreparedStatement pst = con.prepareStatement(sql1);
				ResultSet res = pst.executeQuery(sql1);
				while(res.next()){
					stud.add(new Student(res.getString("matricule"),res.getString("nom"),res.getString("prenom"),res.getString("sexe"), res.getString("lieuDeNaiss")));
				}
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
		
	}*/
    
    @FXML
    void printStaff(ActionEvent event) {

    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		numFill();
		numFill1();
		numFill2();
		fillDepart.setText("12");
		classStaff.getItems().add("6eme");
		classStaff.getItems().add("5eme");
		classStaff.getItems().add("4eme");
		classStaff.getItems().add("3eme");
		classStaff.getItems().add("2nde");
		classStaff.getItems().add("Pre");
		classStaff.getItems().add("Tle");
		classStaff.getItems().add("");
		
		
		try {
			Connection con = FirstConnection.Connect();
			ResultSet res = con.createStatement().executeQuery("SELECT * FROM inscriptenseignant");
			while(res.next()){
				stud.add(new Registration(res.getString("matieres"),res.getString("name"),res.getString("firstName"),res.getString("sexe"), res.getString("lieuNaiss"),res.getString("poste")));
			}
			res.close();
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		mat.setCellValueFactory(new PropertyValueFactory<Registration, String>("matieres"));
		nom.setCellValueFactory(new PropertyValueFactory<Registration, String>("name"));
		prenom.setCellValueFactory(new PropertyValueFactory<Registration, String>("firstName"));
		sex.setCellValueFactory(new PropertyValueFactory<Registration, String>("sexe"));
		lieu.setCellValueFactory(new PropertyValueFactory<Registration, String>("lieu"));
		poste.setCellValueFactory(new PropertyValueFactory<Registration, String>("poste"));
    	
    	tableView.setItems(stud);
	}

}
