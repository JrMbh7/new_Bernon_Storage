package cm.Project.Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import cm.Project.Classe.Student;
import cm.Project.Database.FirstConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

public class studentsController implements Initializable {

	@FXML
    private Pane ElevesInscrits;

    @FXML
    private Label fiilEleve;

    @FXML
    private Pane GarconsInscrits;

    @FXML
    private Label fillBoy;

    @FXML
    private Pane fillesInscrites;

    @FXML
    private Label fillGirls;

    @FXML
    private Pane ClassesTotal;

    @FXML
    private Label fillClass;

    @FXML
    private ComboBox<String> studClass;
	
	@FXML private TableView <Student> tableView;
    
    @FXML private TableColumn <Student, String> matricule;
    
    @FXML private TableColumn <Student, String> nom;
    
    @FXML private TableColumn <Student, String> prenom;
    
    @FXML private TableColumn <Student, String> sexe;
        
    @FXML private TableColumn <Student, String> lieuDeNaiss;
        
     ObservableList<Student> stud = FXCollections.observableArrayList();
     
     String count = "SELECT COUNT(*) FROM ELEVE";
     String count1 = "SELECT COUNT(*) FROM ELEVE WHERE sexe = 'masculin'";
     String count2 = "SELECT COUNT(*) FROM ELEVE WHERE sexe = 'feminin'";

 	public int numFill(){
		int number = 0;
		try {
			
			Connection con = FirstConnection.Connect();
			PreparedStatement psmt;
			ResultSet res1;
			psmt = con.prepareStatement(count);
			res1 = psmt.executeQuery(count);
			
			while(res1.next()){
				number = res1.getInt("count(*)");
			}

			res1.close();	
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		fiilEleve.setText(Integer.toString(number));
		return number;
	}
 	
 	
 	public int numFill1(){
		int number = 0;
		try {
			
			Connection con = FirstConnection.Connect();
			PreparedStatement psmt1;
			ResultSet res2;
			psmt1 = con.prepareStatement(count1);			
			res2 = psmt1.executeQuery(count1);
			
			while(res2.next()){
				number = res2.getInt("count(*)");
			}
			
			res2.close();
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		fillBoy.setText(Integer.toString(number));
		return number;
	}
 	
 	public int numFill2(){
		int number = 0;
		try {
			
			Connection con = FirstConnection.Connect();
			PreparedStatement psmt;
			ResultSet res1;
			psmt = con.prepareStatement(count2);
			
			res1 = psmt.executeQuery(count2);
			
			while(res1.next()){
				number = res1.getInt("count(*)");
			}

			res1.close();	
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		fillGirls.setText(Integer.toString(number));
		return number;
	}
 	
	public void comboChoice(){

		String sql1 = "SELECT * FROM ELEVE WHERE Classe = '"+studClass.getValue()+"';";
		
		if(studClass.getValue().toString().equals("")){
			initialize(null, null);
		} else {
			stud.clear();
			try {
				Connection con = FirstConnection.Connect();
				PreparedStatement pst = con.prepareStatement(sql1);
				ResultSet res = pst.executeQuery(sql1);
				while(res.next()){
					stud.add(new Student(res.getString("matricule"),res.getString("nom"),res.getString("prenom"),res.getString("sexe"), res.getString("lieuDeNaiss")));
				}
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
		
	}
     
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		numFill();
		numFill1();
		numFill2();
		fillClass.setText("7");
		
		studClass.getItems().add("6eme");
		studClass.getItems().add("5eme");
		studClass.getItems().add("4eme");
		studClass.getItems().add("3eme");
		studClass.getItems().add("2nde");
		studClass.getItems().add("Pre");
		studClass.getItems().add("Tle");
		studClass.getItems().add("");
		
		
		try {
			Connection con = FirstConnection.Connect();
			ResultSet res = con.createStatement().executeQuery("SELECT * FROM eleve");
			while(res.next()){
				stud.add(new Student(res.getString("matricule"),res.getString("nom"),res.getString("prenom"),res.getString("sexe"), res.getString("lieuDeNaiss")));
			}
			res.close();
			
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		matricule.setCellValueFactory(new PropertyValueFactory<Student, String>("matricule"));
    	nom.setCellValueFactory(new PropertyValueFactory<Student, String>("nom"));
    	prenom.setCellValueFactory(new PropertyValueFactory<Student, String>("prenom"));
    	sexe.setCellValueFactory(new PropertyValueFactory<Student, String>("sexe"));
    	lieuDeNaiss.setCellValueFactory(new PropertyValueFactory<Student, String>("lieuDeNaiss"));
    	
    	tableView.setItems(stud);
	}
	

}