package cm.Project.Classe;


import javafx.beans.property.SimpleStringProperty;

public class Note {
	
		private SimpleStringProperty matricules, noms, notes, coefs, total, appreciation;

		public Note(String matricules, String noms, String notes, String coefs, String total, String appreciation) {

			this.matricules = new SimpleStringProperty(matricules);
			this.noms = new SimpleStringProperty(noms);
			this.notes = new SimpleStringProperty(notes);
			this.coefs = new SimpleStringProperty(coefs);
			this.total = new SimpleStringProperty(total);
			this.appreciation = new SimpleStringProperty(appreciation);
		}

		public String getMatricules() {
			return matricules.get();
		}

		public void setMatricules(SimpleStringProperty matricules) {
			this.matricules = matricules;
		}

		public String getNoms() {
			return noms.get();
		}

		public void setNoms(SimpleStringProperty noms) {
			this.noms = noms;
		}

		public String getNotes() {
			return notes.get();
		}

		public void setNotes(String notes) {
			this.notes = new SimpleStringProperty(notes);
		}

		public String getCoefs() {
			return coefs.get();
		}

		public void setCoefs(String coefs) {
			this.coefs = new SimpleStringProperty(coefs);;
		}

		public String getTotal() {
			return total.get();
		}

		public void setTotal(String total) {
			this.total = new SimpleStringProperty(total);
		}

		public String getAppreciation() {
			return appreciation.get();
		}

		public void setAppreciation(String appreciation) {
			this.appreciation = new SimpleStringProperty(appreciation);
		}

		
		
}
