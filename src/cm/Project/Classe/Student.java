package cm.Project.Classe;
import javafx.beans.property.SimpleStringProperty;

public class Student {
	private  SimpleStringProperty matricule, nom, prenom, sexe, lieuDeNaiss;
	//private  Date birthday;
	
	public Student(String matricule, String nom, String prenom,String sexe, String lieuDeNaiss) {
		this.matricule = new SimpleStringProperty(matricule);
		this.nom = new SimpleStringProperty(nom);
		this.prenom = new SimpleStringProperty(prenom);
		this.sexe = new SimpleStringProperty(sexe);
		this.lieuDeNaiss = new SimpleStringProperty(lieuDeNaiss);
	}

	public String getMatricule() {
		return matricule.get();
	}

	public void setMatricule(SimpleStringProperty matricule) {
		this.matricule = matricule;
	}

	public String getNom() {
		return nom.get();
	}

	public void setNom(SimpleStringProperty nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom.get();
	}

	public void setPrenom(SimpleStringProperty prenom) {
		this.prenom = prenom;
	}

	public String getSexe() {
		return sexe.get();
	}

	public void setSexe(SimpleStringProperty sexe) {
		this.sexe = sexe;
	}

	public String getLieuDeNaiss() {
		return lieuDeNaiss.get();
	}

	public void setLieuDeNaiss(SimpleStringProperty lieuDeNaiss) {
		this.lieuDeNaiss = lieuDeNaiss;
	}

}
