package cm.Project.Classe;


import javafx.beans.property.SimpleStringProperty;

public class Registration {
	
		private SimpleStringProperty matieres, name, lastName, sexe, lieu, poste;
		//private LocalDate date;
		
		public Registration(String matieres, String name, String lastName, String sexe, String lieu, String poste) {
		
			this.matieres = new SimpleStringProperty(matieres);
			this.name = new SimpleStringProperty(name);
			this.lastName = new SimpleStringProperty(lastName);
			this.sexe = new SimpleStringProperty(sexe);
			this.lieu = new SimpleStringProperty(lieu);
			this.poste = new SimpleStringProperty(poste);
			//this.date = date;
		}

		public String getMatieres() {
			return matieres.get();
		}

		public void setMatieres(SimpleStringProperty matieres) {
			this.matieres = matieres;
		}

		public String getName() {
			return name.get();
		}

		public void setName(SimpleStringProperty name) {
			this.name = name;
		}

		public String getLastName() {
			return lastName.get();
		}

		public void setLastName(SimpleStringProperty lastName) {
			this.lastName = lastName;
		}

		public String getSexe() {
			return sexe.get();
		}

		public void setSexe(SimpleStringProperty sexe) {
			this.sexe = sexe;
		}

		public String getLieu() {
			return lieu.get();
		}

		public void setLieu(SimpleStringProperty lieu) {
			this.lieu = lieu;
		}

		public String getPoste() {
			return poste.get();
		}

		public void setPoste(SimpleStringProperty poste) {
			this.poste = poste;
		}

		/*public LocalDate getDate() {
			return date;
		}

		public void setDate(LocalDate date) {
			this.date = date;
		}*/
}
