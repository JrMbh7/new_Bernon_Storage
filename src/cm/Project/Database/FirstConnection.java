package cm.Project.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.TimeZone;

public class FirstConnection {
	
	public static Connection Connect() throws SQLException, ClassNotFoundException{
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.print("Connecting to a selected database... ");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bernonstorage?useLegacyDatetimeCode=false&amp&serverTimezone=" + TimeZone.getDefault().getID(), "root", "");
			System.out.println("Success!");
			return conn;
		} catch(SQLException  ex){
			ex.printStackTrace();
		}
		return null;
			
	}

}
